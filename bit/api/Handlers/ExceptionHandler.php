<?php

namespace Api\Handlers;

class ExceptionHandler
{
    protected $exception;
    private $exceptionDetails = [];
    protected $logger;


    public function __construct($exception)
    {
        $this->exception = $exception;
    }


    public function handle()
    {
        $exceptionDetails['code'] = $this->exception->getCode();
        $exceptionDetails['message'] = $this->exception->getMessage();
        $exceptionDetails['line'] = $this->exception->getLine();
        $exceptionDetails['file'] = $this->exception->getFile();
       // $exceptionDetails['trace']=explode("\n", $this->exception->getTraceAsString());
        return $exceptionDetails;
    }


}