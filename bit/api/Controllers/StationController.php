<?php
namespace Api\Controllers;

use Api\Data\Models\Passenger;
use PDOException;
use Api\Data\DAO\StationDAO;
use App\Handlers\ApiError;
use Api\Transformers\ResponseTransformer;

class StationController extends \Api\Controllers\Controller
{

    public function __construct($container)
    {
        parent::__construct($container);
    }

    public function getNearestStation($request, $response)
    {

        $longtitude = $request->getParam('longtitude');
        $lattitude = $request->getParam('lattitude');


        $entityDAO=new StationDAO();
        $record=$entityDAO->getNearestStation($lattitude,$longtitude);
        $payload = array (
            'code' => '200',
            'message' => 'Success',
            'result' => $record
        );
        return $response->withJson($payload);
    }

}
