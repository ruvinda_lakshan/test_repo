<?php
namespace Api\Controllers;

use Api\Data\Models\Passenger;
use PDOException;
use Api\Data\DAO\PassengerDAO;
use App\Handlers\ApiError;
use Respect\Validation\Validator as v;
use Api\Transformers\ResponseTransformer;

use \Firebase\JWT\JWT;

class PassengerController extends \Api\Controllers\Controller
{


    public function __construct($container)
    {
        parent::__construct($container);
    }


    public function register($request, $response)
    {
        $title = $request->getParam('title');
        $fname = $request->getParam('fname');
        $lname = $request->getParam('lname');
        $phone = $request->getParam('phone');
        $email = $request->getParam('email');
        $password = $request->getParam('password');
        $country_id = (int)$request->getParam('country_id');


        $passenger=new Passenger();
        $passenger->setTitle($title);
        $passenger->setFname($fname);
        $passenger->setLname($lname);
        $passenger->setPhone($phone);
        $passenger->setEmail($email);
        $passenger->setPassword(password_hash($password,PASSWORD_DEFAULT));
        $passenger->setCountryId($country_id);
        $current_date=date("Y-m-d H:i");
        $passenger->setUpdatedAt($current_date);
        $passenger->setCreatedAt($current_date);


        $passengerDAO=new PassengerDAO();
        $id=$passengerDAO->save($passenger);
        $passenger->setId($id);

        $payload = array (
            'code' => '200',
            'message' => 'Success',
            'result' => null
        );
        return $response->withJson($payload);

    }
    public function  update($request,$response)
    {
        $id =(int) $request->getParam('id');
        $title = $request->getParam('title');
        $fname = $request->getParam('fname');
        $lname = $request->getParam('lname');
        $phone = $request->getParam('phone');
        $email = $request->getParam('email');
        $country_id = (int)$request->getParam('country_id');
        if($country_id==0) $country_id=200;
        $passenger=new Passenger();
        $passenger->setTitle($title);
        $passenger->setFname($fname);
        $passenger->setLname($lname);
        $passenger->setPhone($phone);
        $passenger->setEmail($email);
        $passenger->setCountryId($country_id);
        $current_date=date("Y-m-d H:i");
        $passenger->setUpdatedAt($current_date);
        $passenger->setId($id);

        $passengerDAO=new PassengerDAO();
        $passengerDAO->save($passenger);


        $payload = array (
            'code' => '200',
            'message' => 'Success',
            'result' => null
        );
        return $response->withJson($payload);
    }
}
