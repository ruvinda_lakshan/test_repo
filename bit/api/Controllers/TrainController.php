<?php
namespace Api\Controllers;

use Api\Data\Models\Passenger;
use PDOException;
use Api\Data\DAO\StationDAO;
use App\Handlers\ApiError;
use Api\Transformers\ResponseTransformer;

class TrainController extends \Api\Controllers\Controller
{

    public function __construct($container)
    {
        parent::__construct($container);
    }

    public function setTrainLocation($request, $response)
    {

        $train_id = $request->getParam('train_id');
        $longtitude = $request->getParam('longtitude');
        $lattitude = $request->getParam('lattitude');


        $entityDAO=new StationDAO();
        $record=$entityDAO->getNearestStation($lattitude,$longtitude);
        $payload = array (
            'code' => '200',
            'message' => 'Success',
            'result' => $record
        );
        return $response->withJson($payload);
    }

}
