<?php
namespace Api\Controllers\Auth;

use Api\Data\Models\Passenger;
use PDOException;
use Api\Data\DAO\PassengerDAO;
use App\Handlers\ApiError;
use Respect\Validation\Validator as v;
use Api\Transformers\ResponseTransformer;

use \Firebase\JWT\JWT;

class AuthController extends \Api\Controllers\Controller
{

    public function __construct($container)
    {
        parent::__construct($container);
    }


    public function authenticate($request,$response)
    {
        $email = $request->getParam('email');
        $password = $request->getParam('password');

        $passengerDAO=new PassengerDAO();
        $user = $passengerDAO->isExist($email);


        if (!$user) {
            throw new \Exception('User does not exist');
        }
        if (password_verify($password, $user->getPassword()))
        {
            $token = $passengerDAO->getToken((int)$user->getId());

                if ($token->getId()) {
                   $passenger= $passengerDAO->get((int)$user->getId());
                    $payload = array (
                        'code' => '200',
                        'message' => 'Success',
                        'result' => $passenger,
                        "token"      => $token->getValue()
                    );
                    return $response->withJson($payload);
                }

            if (!$token->getId()) {

                $key = $this->container->get('settings')['jwt']['secret_key'];
                $payload = array(
                    "iss"     => $this->container->get('settings')['jwt']['iss'],
                    "iat"     => time(),
                    "exp"     => time() + (3600 * 24 * 15),
                    "context" => [
                        "user" => [
                            "user_login" => $user->getEmail(),
                            "user_id"    => $user->getId()
                        ]
                    ]
                );


                try {
                    $jwt = JWT::encode($payload, $key,'HS256');


                } catch (\Exception $e) {
                    throw new \Exception('JWT Encode Error');
                }

                $token->setUserId($user->getId());
                $token->setDateCreated($payload['iat']);
                $token->setDateExpiration($payload['exp']);
                $token->setValue($jwt);

                $passengerDAO->saveToken($token);
                $passenger= $passengerDAO->get((int)$user->getId());
                $payload = array (
                    'code' => '200',
                    'message' => 'Success',
                    'result' => $passenger,
                    "token"      =>$jwt
                );
                return $response->withJson($payload);

            }

        }else
        {
            $payload = array (
                'code' => '400',
                'message' => 'Unauthorized',
                'result' => null
            );
            return $response->withJson($payload);
        }
    }

    public function checkToken($request,$response)
    {
        $jwt = $request->getQueryParams();


        $key = $this->container->get('settings')['jwt']['secret_key'];

        try {
            $decoded = JWT::decode($jwt['token'], $key, array('HS256'));
            

        } catch (\Exception $e) {
            //$this->container->logger->info(json_encode($e->getMessage()));
            throw new \Exception('Token not valid or not assigned');
        }
        if (isset($decoded)) {
            $passengerDAO=new PassengerDAO();
            $token = $passengerDAO->getToken((int)$decoded->context->user->user_id);

            if ($token->getId()) {
                return true;
                }
        }
        return false;
    }


    public function isEmailExist($email)
    {
        $passengerDAO=new PassengerDAO();
        $isEmail=$passengerDAO->isEmailExist($email);
        if($isEmail)
        {
            return true;
        }
        return false;
    }



}
