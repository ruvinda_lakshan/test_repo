<?php
namespace Api\Data\Models;


class Station  extends BaseModel {
    private $_id = null;
    private $_code = null;
    private $_name = null;
    private $_description = null;
    private $_station_type = null;
    private $_longitude = null;
    private $_latitude = null;
    private $_city_id = null;
    private $_contact_phone = null;

    public function __construct()
    {
        parent::__construct();
    }


    /**
     * @return null
     */
    public function getId()
    {
        return $this->_id;
    }

    /**
     * @param null $id
     */
    public function setId($id)
    {
        $this->_id = $id;
    }

    /**
     * @return null
     */
    public function getCode()
    {
        return $this->_code;
    }

    /**
     * @param null $code
     */
    public function setCode($code)
    {
        $this->_code = $code;
    }

    /**
     * @return null
     */
    public function getName()
    {
        return $this->_name;
    }

    /**
     * @param null $name
     */
    public function setName($name)
    {
        $this->_name = $name;
    }

    /**
     * @return null
     */
    public function getDescription()
    {
        return $this->_description;
    }

    /**
     * @param null $description
     */
    public function setDescription($description)
    {
        $this->_description = $description;
    }

    /**
     * @return null
     */
    public function getStationType()
    {
        return $this->_station_type;
    }

    /**
     * @param null $station_type
     */
    public function setStationType($station_type)
    {
        $this->_station_type = $station_type;
    }

    /**
     * @return null
     */
    public function getLongitude()
    {
        return $this->_longitude;
    }

    /**
     * @param null $longitude
     */
    public function setLongitude($longitude)
    {
        $this->_longitude = $longitude;
    }

    /**
     * @return null
     */
    public function getLatitude()
    {
        return $this->_latitude;
    }

    /**
     * @param null $latitude
     */
    public function setLatitude($latitude)
    {
        $this->_latitude = $latitude;
    }

    /**
     * @return null
     */
    public function getCityId()
    {
        return $this->_city_id;
    }

    /**
     * @param null $city_id
     */
    public function setCityId($city_id)
    {
        $this->_city_id = $city_id;
    }

    /**
     * @return null
     */
    public function getContactPhone()
    {
        return $this->_contact_phone;
    }

    /**
     * @param null $contact_phone
     */
    public function setContactPhone($contact_phone)
    {
        $this->_contact_phone = $contact_phone;
    }



}
?>