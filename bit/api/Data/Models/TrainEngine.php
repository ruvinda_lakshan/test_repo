<?php
namespace Api\Data\Models;


class TrainEngine extends BaseModel {
    private $_id = null;
    private $_train_id = null;

    public function __construct()
    {
        parent::__construct();
    }
    /**
     * @return null
     */
    public function getId()
    {
        return $this->_id;
    }

    /**
     * @param null $id
     */
    public function setId($id)
    {
        $this->_id = $id;
    }

    /**
     * @return null
     */
    public function getTrainId()
    {
        return $this->_train_id;
    }

    /**
     * @param null $train_id
     */
    public function setTrainId($train_id)
    {
        $this->_train_id = $train_id;
    }


}
?>