<?php
namespace Api\Data\Models;


class TrackingDevice  extends BaseModel {
    private $_id = null;
    private $_code = null;
    private $_train_engine_id = null;
    private $_active = null;

    public function __construct()
    {
        parent::__construct();
    }

    /**
     * @return null
     */
    public function getId()
    {
        return $this->_id;
    }

    /**
     * @param null $id
     */
    public function setId($id)
    {
        $this->_id = $id;
    }

    /**
     * @return null
     */
    public function getCode()
    {
        return $this->_code;
    }

    /**
     * @param null $code
     */
    public function setCode($code)
    {
        $this->_code = $code;
    }

    /**
     * @return null
     */
    public function getTrainEngineId()
    {
        return $this->_train_engine_id;
    }

    /**
     * @param null $train_engine_id
     */
    public function setTrainEngineId($train_engine_id)
    {
        $this->_train_engine_id = $train_engine_id;
    }

    /**
     * @return null
     */
    public function getActive()
    {
        return $this->_active;
    }

    /**
     * @param null $active
     */
    public function setActive($active)
    {
        $this->_active = $active;
    }


}
?>