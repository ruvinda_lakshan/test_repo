<?php
namespace Api\Data\Models;


class RouteHasStation  extends BaseModel {
    private $_id = null;
    private $_route_id = null;
    private $_station_id = null;

    /**
     * @return null
     */
    public function getId()
    {
        return $this->_id;
    }

    /**
     * @param null $id
     */
    public function setId($id)
    {
        $this->_id = $id;
    }

    /**
     * @return null
     */
    public function getRouteId()
    {
        return $this->_route_id;
    }

    /**
     * @param null $route_id
     */
    public function setRouteId($route_id)
    {
        $this->_route_id = $route_id;
    }

    /**
     * @return null
     */
    public function getStationId()
    {
        return $this->_station_id;
    }

    /**
     * @param null $station_id
     */
    public function setStationId($station_id)
    {
        $this->_station_id = $station_id;
    }

    /**
     * @return null
     */
    public function getOrder()
    {
        return $this->_order;
    }

    /**
     * @param null $order
     */
    public function setOrder($order)
    {
        $this->_order = $order;
    }
    private $_order = null;

    public function __construct()
    {
        parent::__construct();
    }



}
?>