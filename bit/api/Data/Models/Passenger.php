<?php

namespace Api\Data\Models;


class Passenger extends BaseModel{
    private $_id = null;
    private $_title = null;
    private $_fname = null;
    private $_lname = null;
    private $_phone = null;
    private $_email = null;
    private $_password = null;
    private $_country_id = null;
    private $_image = null;
    private $_active = null;
    private $_updated_at = null;
    private $_created_at = null;
    /**
     * @return null
     */
    public function getId()
    {
        return $this->_id;
    }

    /**
     * @param null $id
     */
    public function setId($id)
    {
        $this->_id = $id;
    }


    /**
     * @return null
     */
    public function getTitle()
    {
        return $this->_title;
    }

    /**
     * @param null $name
     */
    public function setTitle($name)
    {
        $this->_title = $name;
    }
    /**
     * @return null
     */
    public function getFname()
    {
        return $this->_fname;
    }

    /**
     * @param null $name
     */
    public function setFname($name)
    {
        $this->_fname = $name;
    }
    /**
     * @return null
     */
    public function getLname()
    {
        return $this->_lname;
    }

    /**
     * @param null $name
     */
    public function setLname($name)
    {
        $this->_lname = $name;
    }
    /**
     * @return null
     */
    public function getPhone()
    {
        return $this->_phone;
    }

    /**
     * @param null $phone
     */
    public function setPhone($phone)
    {
        $this->_phone = $phone;
    }

    /**
     * @return null
     */
    public function getEmail()
    {
        return $this->_email;
    }

    /**
     * @param null $email
     */
    public function setEmail($email)
    {
        $this->_email = $email;
    }

    /**
     * @return null
     */
    public function getPassword()
    {
        return $this->_password;
    }

    /**
     * @param null $password
     */
    public function setPassword($password)
    {
        $this->_password = $password;
    }

    /**
     * @return null
     */
    public function getCountryId()
    {
        return $this->_country_id;
    }

    /**
     * @param null $country_id
     */
    public function setCountryId($country_id)
    {
        $this->_country_id = $country_id;
    }

    /**
     * @return null
     */
    public function getImage()
    {
        return $this->_image;
    }

    /**
     * @param null $image
     */
    public function setImage($image)
    {
        $this->_image = $image;
    }

    /**
     * @return null
     */
    public function getActive()
    {
        return $this->_active;
    }

    /**
     * @param null $active
     */
    public function setActive($active)
    {
        $this->_active = $active;
    }

    /**
     * @return null
     */
    public function getUpdatedAt()
    {
        return $this->_updated_at;
    }

    /**
     * @param null $updated_at
     */
    public function setUpdatedAt($updated_at)
    {
        $this->_updated_at = $updated_at;
    }

    /**
     * @return null
     */
    public function getCreatedAt()
    {
        return $this->_created_at;
    }

    /**
     * @param null $created_at
     */
    public function setCreatedAt($created_at)
    {
        $this->_created_at = $created_at;
    }




}