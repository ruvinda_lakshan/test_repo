<?php
namespace Api\Data\Models;


class TrainLocation  extends BaseModel {

    private $_id=null;
    private $_device_id=null;
    private $_lattitude=null;
    private $_longtitude=null;
    private $_updated_time=null;
    private $_system_time=null;


    public function __construct()
    {
        parent::__construct();
    }

    /**
     * @return null
     */
    public function getId()
    {
        return $this->_id;
    }

    /**
     * @param null $id
     */
    public function setId($id)
    {
        $this->_id = $id;
    }

    /**
     * @return null
     */
    public function getDeviceId()
    {
        return $this->_device_id;
    }

    /**
     * @param null $device_id
     */
    public function setDeviceId($device_id)
    {
        $this->_device_id = $device_id;
    }

    /**
     * @return null
     */
    public function getLattitude()
    {
        return $this->_lattitude;
    }

    /**
     * @param null $lattitude
     */
    public function setLattitude($lattitude)
    {
        $this->_lattitude = $lattitude;
    }

    /**
     * @return null
     */
    public function getLongtitude()
    {
        return $this->_longtitude;
    }

    /**
     * @param null $longtitude
     */
    public function setLongtitude($longtitude)
    {
        $this->_longtitude = $longtitude;
    }

    /**
     * @return null
     */
    public function getUpdatedTime()
    {
        return $this->_updated_time;
    }

    /**
     * @param null $updated_time
     */
    public function setUpdatedTime($updated_time)
    {
        $this->_updated_time = $updated_time;
    }

    /**
     * @return null
     */
    public function getSystemTime()
    {
        return $this->_system_time;
    }

    /**
     * @param null $system_time
     */
    public function setSystemTime($system_time)
    {
        $this->_system_time = $system_time;
    }





}
?>