<?php
namespace Api\Data\Models;


class State  extends BaseModel {
    private $_id = null;
    private $_name = null;
    private $_country_id = null;

    public function __construct()
    {
        parent::__construct();
    }

    /**
     * @return null
     */
    public function getId()
    {
        return $this->_id;
    }

    /**
     * @param null $id
     */
    public function setId($id)
    {
        $this->_id = $id;
    }

    /**
     * @return null
     */
    public function getName()
    {
        return $this->_name;
    }

    /**
     * @param null $name
     */
    public function setName($name)
    {
        $this->_name = $name;
    }

    /**
     * @return null
     */
    public function getCountryId()
    {
        return $this->_country_id;
    }

    /**
     * @param null $country_id
     */
    public function setCountryId($country_id)
    {
        $this->_country_id = $country_id;
    }



}
?>