<?php
namespace Api\Data\Models;


class PassengerTravelBookmark  extends BaseModel {
    private $_id = null;
    private $_passenger_id = null;
    private $_name = null;
    private $_type = null;
    private $_referance_id = null;
    private $_active = null;


    public function __construct()
    {
        parent::__construct();
    }

    /**
     * @return null
     */
    public function getId()
    {
        return $this->_id;
    }

    /**
     * @param null $id
     */
    public function setId($id)
    {
        $this->_id = $id;
    }

    /**
     * @return null
     */
    public function getPassengerId()
    {
        return $this->_passenger_id;
    }

    /**
     * @param null $passenger_id
     */
    public function setPassengerId($passenger_id)
    {
        $this->_passenger_id = $passenger_id;
    }

    /**
     * @return null
     */
    public function getName()
    {
        return $this->_name;
    }

    /**
     * @param null $name
     */
    public function setName($name)
    {
        $this->_name = $name;
    }

    /**
     * @return null
     */
    public function getType()
    {
        return $this->_type;
    }

    /**
     * @param null $type
     */
    public function setType($type)
    {
        $this->_type = $type;
    }

    /**
     * @return null
     */
    public function getReferanceId()
    {
        return $this->_referance_id;
    }

    /**
     * @param null $referance_id
     */
    public function setReferanceId($referance_id)
    {
        $this->_referance_id = $referance_id;
    }

    /**
     * @return null
     */
    public function getActive()
    {
        return $this->_active;
    }

    /**
     * @param null $active
     */
    public function setActive($active)
    {
        $this->_active = $active;
    }


}
?>