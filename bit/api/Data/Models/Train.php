<?php
namespace Api\Data\Models;


class Train  extends BaseModel {
    private $_id = null;
    private $_name = null;
    private $_train_type = null;

    public function __construct()
    {
        parent::__construct();
    }
    /**
     * @return null
     */
    public function getId()
    {
        return $this->_id;
    }

    /**
     * @param null $id
     */
    public function setId($id)
    {
        $this->_id = $id;
    }

    /**
     * @return null
     */
    public function getName()
    {
        return $this->_name;
    }

    /**
     * @param null $name
     */
    public function setName($name)
    {
        $this->_name = $name;
    }

    /**
     * @return null
     */
    public function getTrainType()
    {
        return $this->_train_type;
    }

    /**
     * @param null $train_type
     */
    public function setTrainType($train_type)
    {
        $this->_train_type = $train_type;
    }




}
?>