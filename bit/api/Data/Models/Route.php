<?php
namespace Api\Data\Models;


class Route  extends BaseModel {
    private $_id = null;
    private $_name = null;
    private $_description = null;
    private $_active = null;

    public function __construct()
    {
        parent::__construct();
    }

    /**
     * @return null
     */
    public function getId()
    {
        return $this->_id;
    }

    /**
     * @param null $id
     */
    public function setId($id)
    {
        $this->_id = $id;
    }

    /**
     * @return null
     */
    public function getName()
    {
        return $this->_name;
    }

    /**
     * @param null $name
     */
    public function setName($name)
    {
        $this->_name = $name;
    }

    /**
     * @return null
     */
    public function getDescription()
    {
        return $this->_description;
    }

    /**
     * @param null $description
     */
    public function setDescription($description)
    {
        $this->_description = $description;
    }

    /**
     * @return null
     */
    public function getActive()
    {
        return $this->_active;
    }

    /**
     * @param null $active
     */
    public function setActive($active)
    {
        $this->_active = $active;
    }


}
?>