<?php
namespace Api\Data\Models;


class Token  extends BaseModel {
    private $_id = null;
    private $_value = null;
    private $_user_id = null;
    private $_date_created = null;

    public function __construct()
    {
        parent::__construct();
    }

    /**
     * @return null
     */
    public function getId()
    {
        return $this->_id;
    }

    /**
     * @param null $id
     */
    public function setId($id)
    {
        $this->_id = $id;
    }

    /**
     * @return null
     */
    public function getValue()
    {
        return $this->_value;
    }

    /**
     * @param null $value
     */
    public function setValue($value)
    {
        $this->_value = $value;
    }

    /**
     * @return null
     */
    public function getUserId()
    {
        return $this->_user_id;
    }

    /**
     * @param null $user_id
     */
    public function setUserId($user_id)
    {
        $this->_user_id = $user_id;
    }

    /**
     * @return null
     */
    public function getDateCreated()
    {
        return $this->_date_created;
    }

    /**
     * @param null $date_created
     */
    public function setDateCreated($date_created)
    {
        $this->_date_created = $date_created;
    }

    /**
     * @return null
     */
    public function getDateExpiration()
    {
        return $this->_date_expiration;
    }

    /**
     * @param null $date_expiration
     */
    public function setDateExpiration($date_expiration)
    {
        $this->_date_expiration = $date_expiration;
    }
    private $_date_expiration = null;





}
?>