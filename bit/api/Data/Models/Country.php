<?php
namespace Api\Data\Models;


class Country  extends BaseModel {
    private $_id = null;
    private $_iso = null;
    private $_name = null;
    private $_nicename = null;
    private $_iso3 = null;
    private $_numcode = null;

    public function __construct()
    {
        parent::__construct();
    }

    /**
     * @return null
     */
    public function getId()
    {
        return $this->_id;
    }

    /**
     * @param null $id
     */
    public function setId($id)
    {
        $this->_id = $id;
    }

    /**
     * @return null
     */
    public function getIso()
    {
        return $this->_iso;
    }

    /**
     * @param null $iso
     */
    public function setIso($iso)
    {
        $this->_iso = $iso;
    }

    /**
     * @return null
     */
    public function getName()
    {
        return $this->_name;
    }

    /**
     * @param null $name
     */
    public function setName($name)
    {
        $this->_name = $name;
    }

    /**
     * @return null
     */
    public function getNicename()
    {
        return $this->_nicename;
    }

    /**
     * @param null $nicename
     */
    public function setNicename($nicename)
    {
        $this->_nicename = $nicename;
    }

    /**
     * @return null
     */
    public function getIso3()
    {
        return $this->_iso3;
    }

    /**
     * @param null $iso3
     */
    public function setIso3($iso3)
    {
        $this->_iso3 = $iso3;
    }

    /**
     * @return null
     */
    public function getNumcode()
    {
        return $this->_numcode;
    }

    /**
     * @param null $numcode
     */
    public function setNumcode($numcode)
    {
        $this->_numcode = $numcode;
    }


}
?>