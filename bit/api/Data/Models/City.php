<?php
namespace Api\Data\Models;


class City  extends BaseModel {
    private $_id = null;
    private $_name = null;
    private $_state_id = null;

    public function __construct()
    {
        parent::__construct();
    }

    /**
     * @return null
     */
    public function getId()
    {
        return $this->_id;
    }

    /**
     * @param null $id
     */
    public function setId($id)
    {
        $this->_id = $id;
    }

    /**
     * @return null
     */
    public function getName()
    {
        return $this->_name;
    }

    /**
     * @param null $name
     */
    public function setName($name)
    {
        $this->_name = $name;
    }

    /**
     * @return null
     */
    public function getStateId()
    {
        return $this->_state_id;
    }

    /**
     * @param null $state_id
     */
    public function setStateId($state_id)
    {
        $this->_state_id = $state_id;
    }


}
?>