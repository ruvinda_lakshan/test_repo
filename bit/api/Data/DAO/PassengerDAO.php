<?php

namespace Api\Data\DAO;
use Api\Data\Models\Passenger;
use Api\Data\Models\Token;
use PDO;

class PassengerDAO extends  DAO{

    protected $passenger='passenger';
    protected $token='token';
    function save(Passenger $data) {
        //$data=(Passenger)$passenger;

        if(is_null($data->getId())) {
            $sql='INSERT INTO  '.$this->passenger.'(
            title,
            fname,
            lname,
            phone,
            email,
            password,
            country_id,
            updated_at,
            created_at
            ) VALUES (
            :title,
            :fname,
            :lname,
            :phone,
            :email,
            :password,
            :country_id,
            :updated_at,
            :created_at
            );';


            $array = array(
                'title' => $data->getTitle(),
                'fname' => $data->getFname(),
                'lname' => $data->getLname(),
                'phone' => $data->getPhone(),
                'email' => $data->getEmail(),
                'password' => $data->getPassword(),
                'country_id' => $data->getCountryId(),
                'updated_at' => $data->getUpdatedAt(),
                'created_at' => $data->getCreatedAt()
            );


            $typeArray = array(
                'title' => PDO::PARAM_STR,
                'fname' => PDO::PARAM_STR,
                'lname' => PDO::PARAM_STR,
                'phone' => PDO::PARAM_INT,
                'email' => PDO::PARAM_STR,
                'password' => PDO::PARAM_STR,
                'country_id' => PDO::PARAM_INT,
                'updated_at' => PDO::PARAM_STR,
                'created_at' => PDO::PARAM_STR
            );
            $id=$this->adapter->insert($sql,$array,$typeArray);
            return $id;
        }
        else {
           $sql='UPDATE '.$this->passenger.' SET
           title=:title,
           fname=:fname,
           lname=:lname,
           phone=:phone,
           email=:email,
           country_id=:country_id,
           updated_at=:updated_at WHERE id=:id LIMIT 1;';

            $array = array(
                'title' => $data->getTitle(),
                'fname' => $data->getFname(),
                'lname' => $data->getLname(),
                'phone' => $data->getPhone(),
                'email' => $data->getEmail(),
                'country_id' => $data->getCountryId(),
                'updated_at' => $data->getUpdatedAt(),
                'id'=>$data->getId()
            );


            $typeArray = array(
                'title' => PDO::PARAM_STR,
                'fname' => PDO::PARAM_STR,
                'lname' => PDO::PARAM_STR,
                'phone' => PDO::PARAM_INT,
                'email' => PDO::PARAM_STR,
                'country_id' => PDO::PARAM_INT,
                'updated_at' => PDO::PARAM_STR,
                'id' => PDO::PARAM_INT
            );
            $this->adapter->update($sql,$array,$typeArray);
        }
    }

    function delete($db) {
        if(is_null($this->_id)) {
            return;
        }
        else {
            $sql = 'DELETE FROM '.$this->passenger.'  WHERE id=:id LIMIT 1;';
            $array = array(
                'id' =>  $this->id
            );

            $typeArray = array(
                'id' => PDO::PARAM_INT
            );
            $this->adapter->delete($sql,$array,$typeArray);
        }
    }

   /* function get($id) {
        if(gettype($id) != "integer") {
            throw new \Exception("Value not integer");
        }
        else {
            $sql = 'SELECT * FROM '.$this->passenger.' WHERE id = :id';
            $array = array(
                'id' =>  $this->id
            );

            $typeArray = array(
                'id' => PDO::PARAM_INT
            );
            $data=$this->adapter->query($sql,$array,$typeArray);
            return $data;
        }
    }*/
    public function get($input)
    {
        if (gettype($input) != "integer") {
            throw new \Exception("Value not Number");
        } else {
            $sql = 'SELECT * FROM '.$this->passenger.' WHERE id = :id limit 1';
            $array = array(
                'id' =>  $input
            );

            $typeArray = array(
                'id' => PDO::PARAM_INT
            );
            $records=$this->adapter->query($sql,$array,$typeArray);
            // print_r($records);exit();


            //$passenger=new Passenger();
            $passenger=[];

            if(count($records)==1)
            {

                $passenger['id']=$records[0]['id'];
                $passenger['title']=$records[0]['title'];
                $passenger['fname']=$records[0]['fname'];
                $passenger['lname']=$records[0]['lname'];
                $passenger['phone']=$records[0]['phone'];
                $passenger['email']=$records[0]['email'];
                $passenger['country_id']=$records[0]['country_id'];
                $passenger['image']=$records[0]['image'];
                $passenger['active']=$records[0]['active'];
            }
           return $passenger;
        }
    }

    public function isExist($email)
    {
        if (gettype($email) != "string") {
            throw new \Exception("Value not text");
        } else {
            $sql = 'SELECT * FROM '.$this->passenger.' WHERE email = :email limit 1';
            $array = array(
                'email' =>  $email
            );

            $typeArray = array(
                'email' => PDO::PARAM_STR
            );
            $records=$this->adapter->query($sql,$array,$typeArray);
           // print_r($records);exit();
            $passenger=new Passenger();

            if(count($records)==1)
            {
                $passenger->setId($records[0]['id']);
                $passenger->setFname($records[0]['fname']);
                $passenger->setLname($records[0]['lname']);
                $passenger->setPhone($records[0]['phone']);
                $passenger->setEmail($records[0]['email']);
                $passenger->setPassword($records[0]['password']);
                $passenger->setCountryId($records[0]['country_id']);
                $passenger->setImage($records[0]['image']);
                $passenger->setActive($records[0]['active']);
                $passenger->setUpdatedAt($records[0]['updated_at']);
                $passenger->setCreatedAt($records[0]['created_at']);

            }
            return $passenger;
        }
    }

    public function isEmailExist($email)
    {
        if (gettype($email) != "string") {
            throw new \Exception("Value not text");
        } else {
            $sql = 'SELECT * FROM '.$this->passenger.' WHERE email = :email limit 1';
            $array = array(
                'email' =>  $email
            );

            $typeArray = array(
                'email' => PDO::PARAM_STR
            );
            $records=$this->adapter->query($sql,$array,$typeArray);


            if(count($records)==1)
            {
                return true;

            }
            return false;
        }
    }


    public function isPhoneExist($input)
    {
        if (gettype($input) != "integer") {
            throw new \Exception("Value not number");
        } else {
            $sql = 'SELECT * FROM '.$this->passenger.' WHERE phone = :phone limit 1';
            $array = array(
                'phone' =>  $input
            );

            $typeArray = array(
                'phone' => PDO::PARAM_INT
            );
            $records=$this->adapter->query($sql,$array,$typeArray);


            if(count($records)==1)
            {
                return true;

            }
            return false;
        }
    }


    public function getToken($id)
    {
        if (gettype($id) != "integer") {
            throw new \Exception("Value not number");
        } else {
            $sql = 'SELECT * FROM '.$this->token.' WHERE  user_id = :user_id AND date_expiration > :date_expiration';
            $array = array(
                'user_id' =>  $id,
                'date_expiration' =>  time(),
            );
            $typeArray = array(
                'user_id' => PDO::PARAM_INT,
                'date_expiration' => PDO::PARAM_STR
            );
            $records=$this->adapter->query($sql,$array,$typeArray);
            $token=new Token();
            if(count($token)==1)
            {
                $token->setId($records[0]['id']);
                $token->setValue($records[0]['value']);
                $token->setUserId($records[0]['user_id']);
                $token->setDateCreated($records[0]['date_created']);

            }
            return $token;
        }
    }

    public function saveToken(Token $token)
    {
       // $token=(Token)$class;

        if(is_null($token->getId())) {

            // $stmt = $this->db->prepare();
            $sql='INSERT INTO '.$this->token.' (user_id, value, date_created, date_expiration) VALUES (:user_id, :value, :date_created, :date_expiration);';

            $array = array(
                'user_id' => $token->getUserId(),
                'value' => $token->getValue(),
                'date_created' => $token->getDateCreated(),
                'date_expiration' => $token->getDateExpiration()
            );

            $typeArray = array(
                'user_id' => PDO::PARAM_INT,
                'value' => PDO::PARAM_STR,
                'date_created' => PDO::PARAM_STR,
                'date_expiration' => PDO::PARAM_STR,
            );
            $this->adapter->insert($sql,$array,$typeArray);
            //$this->_id= $stmt->insert();
            //$this->_id = $stmt->lastInsertId();
        }
       /* else {
            $sql='UPDATE '.$this->token.' SET value=:value,user_id=:user_id,date_created=:date_created WHERE id=:id LIMIT 1;';

            $array = array(
                'value' => $token->getValue(),
                'user_id' => $token->getUserId(),
                'date_created' => $token->getDateCreated(),
                'id' =>  $token->getId()
            );

            $typeArray = array(
                'value' => PDO::PARAM_STR,
                'user_id' => PDO::PARAM_INT,
                'date_created' => PDO::PARAM_STR,
                'id' => PDO::PARAM_INT
            );
            $this->adapter->update($sql,$array,$typeArray);
        }*/
    }
}