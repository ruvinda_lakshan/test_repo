<?php

namespace Api\Data\DAO;
use Api\Data\Models\TrainLocation;
use PDO;

class TrainLocationDAO extends  DAO{

    protected $table='train_location';

    function save(Station $data) {
        //$data=(Passenger)$passenger;

        if(is_null($data->getId())) {
            $sql='INSERT INTO  '.$this->table.'(
            device_id,
            lattitude,
            longtitude,
            updated_time,
            system_time
            ) VALUES (
           :device_id,
        :lattitude,
        :longtitude,
        :updated_time,
        :system_time
            );';


            $array = array(
                'device_id' => $data->getDevice_Id,
                'lattitude' => $data->getLattitude,
                'longtitude' => $data->getLongtitude,
                'updated_time' => $data->getUpdated_Time,
                'system_time' => $data->getSystem_Time
            );


            $typeArray = array(
                'device_id' => PDO::PARAM_INT,
                'lattitude' => PDO::PARAM_STR,
                'longtitude' => PDO::PARAM_STR,
                'updated_time' => PDO::PARAM_STR,
                'system_time' => PDO::PARAM_STR
            );
            $id=$this->adapter->insert($sql,$array,$typeArray);
            return $id;
        }
        else {
            $sql='UPDATE '.$this->table.' SET
            device_id=:device_id,
            lattitude=:lattitude,
            longtitude=:longtitude,
            updated_time=:updated_time,
            system_time=:system_time,
            WHERE id=:id LIMIT 1;';

            $array = array(
                'device_id' => $data->getDevice_Id,
                'lattitude' => $data->getLattitude,
                'longtitude' => $data->getLongtitude,
                'updated_time' => $data->getUpdated_Time,
                'system_time' => $data->getSystem_Time,
                'id'=>$data->getId()
            );


            $typeArray = array(
                'device_id' => PDO::PARAM_INT,
                'lattitude' => PDO::PARAM_STR,
                'longtitude' => PDO::PARAM_STR,
                'updated_time' => PDO::PARAM_STR,
                'system_time' => PDO::PARAM_STR,
                'id' => PDO::PARAM_INT
            );
            $this->adapter->update($sql,$array,$typeArray);
        }
    }



}