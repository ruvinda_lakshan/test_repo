<?php

namespace Api\Data\DAO;
use Api\Data\Models\Station;
use PDO;

class StationDAO extends  DAO{

    protected $station='station';

    function save(Station $data) {
        //$data=(Passenger)$passenger;

        if(is_null($data->getId())) {
            $sql='INSERT INTO  '.$this->station.'(
            code,
            name,
            description,
            station_type,
            lattitude,
            longtitude,
            city_id,
            contact_phone
            ) VALUES (
            :code,
            :name,
            :description,
            :station_type,
            :lattitude,
            :longtitude,
            :city_id,
            :contact_phone
            );';


            $array = array(
                'code' => $data->getCode,
                'name' => $data->getName,
                'description' => $data->getDescription,
                'station_type' => $data->getStation_Type,
                'lattitude' => $data->getLattitude,
                'longtitude' => $data->getLongtitude,
                'city_id' => $data->getCity_Id,
                'contact_phone' => $data->getContact_Phone
            );


            $typeArray = array(
                'code' => PDO::PARAM_STR,
                'name' => PDO::PARAM_STR,
                'description' => PDO::PARAM_STR,
                'station_type' => PDO::PARAM_STR,
                'lattitude' => PDO::PARAM_STR,
                'longtitude' => PDO::PARAM_STR,
                'city_id' => PDO::PARAM_INT,
                'contact_phone' => PDO::PARAM_STR
            );
            $id=$this->adapter->insert($sql,$array,$typeArray);
            return $id;
        }
        else {
           $sql='UPDATE '.$this->station.' SET
           code=:code,
            name=:name,
            description=:description,
            station_type=:station_type,
            lattitude=:lattitude,
            longtitude=:longtitude,
            city_id=:city_id,
            contact_phone=:contact_phone 
           WHERE id=:id LIMIT 1;';

            $array = array(
                'code' => $data->getCode,
                'name' => $data->getName,
                'description' => $data->getDescription,
                'station_type' => $data->getStation_Type,
                'lattitude' => $data->getLattitude,
                'longtitude' => $data->getLongtitude,
                'city_id' => $data->getCity_Id,
                'contact_phone' => $data->getContact_Phone,
                'id'=>$data->getId()
            );


            $typeArray = array(
                'code' => PDO::PARAM_STR,
                'name' => PDO::PARAM_STR,
                'description' => PDO::PARAM_STR,
                'station_type' => PDO::PARAM_STR,
                'lattitude' => PDO::PARAM_STR,
                'longtitude' => PDO::PARAM_STR,
                'city_id' => PDO::PARAM_INT,
                'contact_phone' => PDO::PARAM_STR,
                'id' => PDO::PARAM_INT
            );
            $this->adapter->update($sql,$array,$typeArray);
        }
    }

    function delete($input) {
        if(is_null($input)) {
            return;
        }
        else {
            $sql = 'DELETE FROM '.$this->station.'  WHERE id=:id LIMIT 1;';
            $array = array(
                'id' =>  $input
            );

            $typeArray = array(
                'id' => PDO::PARAM_INT
            );
            $this->adapter->delete($sql,$array,$typeArray);
        }
    }

    public function get($input)
    {
        if (gettype($input) != "integer") {
            throw new \Exception("Value not Number");
        } else {
            $sql = 'SELECT * FROM '.$this->station.' WHERE id = :id limit 1';
            $array = array(
                'id' =>  $input
            );

            $typeArray = array(
                'id' => PDO::PARAM_INT
            );
            $records=$this->adapter->query($sql,$array,$typeArray);
            // print_r($records);exit();


            //$passenger=new Passenger();
            $data=[];

            if(count($records)==1)
            {

                $data['id']=$records[0]['id'];
                $data['code']=$records[0]['code'];
                $data['name']=$records[0]['name'];
                $data['description']=$records[0]['description'];
                $data['station_type']=$records[0]['station_type'];
                $data['lattitude']=$records[0]['lattitude'];
                $data['longtitude']=$records[0]['longtitude'];
                $data['city_id']=$records[0]['city_id'];
                $data['contact_phone']=$records[0]['contact_phone'];
            }
           return $data;
        }
    }

    public function getNearestStation($lat,$lon)
    {

        $sql='select id from '.$this->station.' where st_distance_sphere(point(longtitude,lattitude), point(:longtitude,:lattitude))<500 limit 1';
        $array = array(
            'longtitude' =>  $lon,
            'lattitude'=>$lat
        );
        $typeArray = array(
            'longtitude' => PDO::PARAM_STR,
            'lattitude' => PDO::PARAM_STR

        );
        $records=$this->adapter->query($sql,$array,$typeArray);
        if(count($records)>0) {
            return $records[0]['id'];
        }
        else {
            return 7;
        }
    }

    public function isExist($input,$type)
    {
        $search = "code=:code";
        $search_field="code";
        $pdo_param="PDO::PARAM_STR";
        switch ($type) {
            case "station_type":
                $search = "station_type=:station_type";
                $search_field="station_type";
                break;
            case "city_id":
                $search = "city_id=:city_id";
                $search_field="city_id";
                $pdo_param="PDO::PARAM_INT";
                break;
            case "contact_phone":
                $search = "contact_phone=:contact_phone";
                $search_field="contact_phone";
                break;
            case "name":
                $search = "name=:name";
                $search_field="name";
                break;
            default:
                $search = "code=:code";
                $search_field="code";
        }

            $sql = 'SELECT * FROM '.$this->station.' WHERE '.$search.' limit 1';
            $array = array(
                ''.$search_field.'' =>  $input
            );

            $typeArray = array(
                ''.$search_field.'' => $pdo_param
            );
            $records=$this->adapter->query($sql,$array,$typeArray);
           // print_r($records);exit();
            $object=new Station();

            if(count($records)==1)
            {
                $passenger->setId($records[0]['id']);
                $passenger->setFname($records[0]['fname']);
                $passenger->setLname($records[0]['lname']);
                $passenger->setPhone($records[0]['phone']);
                $passenger->setEmail($records[0]['email']);
                $passenger->setPassword($records[0]['password']);
                $passenger->setCountryId($records[0]['country_id']);
                $passenger->setImage($records[0]['image']);
                $passenger->setActive($records[0]['active']);
                $passenger->setUpdatedAt($records[0]['updated_at']);
                $passenger->setCreatedAt($records[0]['created_at']);

            }
            return $passenger;

    }



}