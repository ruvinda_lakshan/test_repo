<?php

$settings = [
    'settings' => [
        'displayErrorDetails' => true,

        'db' => [
            'host' => 'localhost',
            'user' => 'root',
            'password' => 'mysql',
            'schema' => 'myproject',
        ],
        'jwt' => [
            'secret_key' => 'mySercretCode',
            'iss'=>'http://local.bit.dev'
        ]
    ]
];
