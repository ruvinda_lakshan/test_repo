<?php
namespace Api\Transformers;

class ResponseTransformer
{
    const HTTP_OK = 200;
    const HTTP_CREATED = 201;
    const HTTP_NO_CONTENT = 204;
    const HTTP_SERVER_ERROR = 500;




    public function transform($response, $data, $statusCode)
    {
        $return = [
            "data" => null,
            "error" => null,
        ];

        switch($statusCode)
        {
            case self::HTTP_OK:
                $return['data'] = $data;
                break;

            case self::HTTP_CREATED:
                $return['data'] = $data;
                break;

            case self::HTTP_NO_CONTENT:
                // no content
                break;

            case self::HTTP_SERVER_ERROR:
                $return['error'] = $data;
                break;

            default:
                $return['error'] = 'Status ' . $statusCode . ' is not supported.';
                break;
        }

        return $response->withJson($return, $statusCode);
    }
}