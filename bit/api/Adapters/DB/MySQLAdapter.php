<?php

namespace Api\Adapters\DB;

use \PDO;
use \PDOException;

class MySQLAdapter extends DBAdapterAbstract
{
    protected $db = null;


// _______________________________________________________________________________________________________ magic methods


    /**
     * MySQLAdapter constructor.
     *
     * @param $config
     */
    public function __construct($config)
    {
        parent::__construct($config);

        // create the connection string
        $connectionString = "mysql:host={$this->host};dbname={$this->schema}";

        // create the db handler
        try
        {
            $this->db = new PDO($connectionString, $this->user, $this->password);

            $this->db->setAttribute(PDO::ATTR_ERRMODE, PDO::ERRMODE_EXCEPTION);
            $this->db->setAttribute(PDO::ATTR_DEFAULT_FETCH_MODE, PDO::FETCH_ASSOC);
        }
        catch (PDOException $e)
        {
            throw $e;
        }
    }


    /**
     * MySQLAdapter Destructor.
     */
    public function __destruct()
    {
        $this->db = null;
    }


// ______________________________________________________________________________________________________________ public


    /**
     * Common SELECT from a single table.
     *
     * @param $table
     * @return string
     */
    public function select($table)
    {
        return "Hello from MySQL " . $table;
    }


    /**
    * Common INSERT in to a single table.
    */
    public function insert($sql,$array,$typeArray)
    {
        $stmt=$this->db->prepare($sql);
        foreach($array as $key => $value)
        {
            if($typeArray)
                $stmt->bindValue(':'.$key.'',$value,$typeArray[$key]);

            else
            {
                if(is_int($value))
                    $param = PDO::PARAM_INT;
                elseif(is_bool($value))
                    $param = PDO::PARAM_BOOL;
                elseif(is_null($value))
                    $param = PDO::PARAM_NULL;
                elseif(is_string($value))
                    $param = PDO::PARAM_STR;
                else
                    $param = FALSE;

                if($param)
                    $stmt->bindValue(':'.$key.'',$value,$param);
            }

        }
        $stmt->execute();
        return $this->db->lastInsertId();
    }


    /**
    * Common UPDATE to a single table.
    */
    public function update($sql,$array,$typeArray)
    {
        $stmt=$this->db->prepare($sql);
        foreach($array as $key => $value)
        {
            if($typeArray)
                $stmt->bindValue(":$key",$value,$typeArray[$key]);

            else
            {
                if(is_int($value))
                    $param = PDO::PARAM_INT;
                elseif(is_bool($value))
                    $param = PDO::PARAM_BOOL;
                elseif(is_null($value))
                    $param = PDO::PARAM_NULL;
                elseif(is_string($value))
                    $param = PDO::PARAM_STR;
                else
                    $param = FALSE;

                if($param)
                    $stmt->bindValue(":$key",$value,$param);
            }
        }
        $stmt->execute();
    }


    /**
    * Common DELETE from a single table.
    */
    public function delete($sql,$array,$typeArray)
    {
        $stmt=$this->db->prepare($sql);
        foreach($array as $key => $value)
        {
            if($typeArray)
                $stmt->bindValue(':'.$key.'',$value,$typeArray[$key]);
            else
            {
                if(is_int($value))
                    $param = PDO::PARAM_INT;
                elseif(is_bool($value))
                    $param = PDO::PARAM_BOOL;
                elseif(is_null($value))
                    $param = PDO::PARAM_NULL;
                elseif(is_string($value))
                    $param = PDO::PARAM_STR;
                else
                    $param = FALSE;

                if($param)
                    $stmt->bindValue(":$key",$value,$param);
            }
        }
        $stmt->execute();
    }


    /**
    * Execute a raw query.
    */
    public function query($sql,$array,$typeArray)
    {
        $stmt=$this->db->prepare($sql);
        foreach($array as $key => $value)
        {
            if($typeArray)
                $stmt->bindValue(":$key",$value,$typeArray[$key]);
            else
            {
                if(is_int($value))
                    $param = PDO::PARAM_INT;
                elseif(is_bool($value))
                    $param = PDO::PARAM_BOOL;
                elseif(is_null($value))
                    $param = PDO::PARAM_NULL;
                elseif(is_string($value))
                    $param = PDO::PARAM_STR;
                else
                    $param = FALSE;

                if($param)
                    $stmt->bindValue(":$key",$value,$param);
            }
        }
        $stmt->execute();
        return $stmt->fetchAll();
    }

    /**
     * Execute a raw query.
     */
    public function rowCount($sql,$array,$typeArray)
    {
        $stmt=$this->db->prepare($sql);
        foreach($array as $key => $value)
        {
            if($typeArray)
                $stmt->bindValue(":$key",$value,$typeArray[$key]);
            else
            {
                if(is_int($value))
                    $param = PDO::PARAM_INT;
                elseif(is_bool($value))
                    $param = PDO::PARAM_BOOL;
                elseif(is_null($value))
                    $param = PDO::PARAM_NULL;
                elseif(is_string($value))
                    $param = PDO::PARAM_STR;
                else
                    $param = FALSE;

                if($param)
                    $stmt->bindValue(":$key",$value,$param);
            }
        }

        $stmt->execute();
        $records = $stmt->rowCount();
        return $records;
    }


// _____________________________________________________________________________________________________________ private


    /**
     * Begin a transaction.
     */
    private function transactionBegin()
    {
        // check already in a transaction and if not start the transaction
        if(!$this->db->inTransaction())
        {
            $this->db->beginTransaction();
        }
    }


    /**
     * Commit the transaction.
     */
    private function transactionCommit()
    {
        $this->db->commit();
    }


    /**
     * Rollback the transaction.
     */
    private function transactionRollback()
    {
        $this->db->rollBack();
    }

/*
    private function bindArrayValue($req, $array, $typeArray = false)
    {
       // if(is_object($req) && ($req instanceof PDOStatement))
       // {
            foreach($array as $key => $value)
            {
                if($typeArray)
                    $this->db->bindValue(":$key",$value,$typeArray[$key]);
                else
                {
                    if(is_int($value))
                        $param = PDO::PARAM_INT;
                    elseif(is_bool($value))
                        $param = PDO::PARAM_BOOL;
                    elseif(is_null($value))
                        $param = PDO::PARAM_NULL;
                    elseif(is_string($value))
                        $param = PDO::PARAM_STR;
                    else
                        $param = FALSE;

                    if($param)
                        $this->db->bindValue(":$key",$value,$param);
                }
            }
       // }
    }
*/
    /**
     * ## EXEMPLE ##
     * $array = array('language' => 'php','lines' => 254, 'publish' => true);
     * $typeArray = array('language' => PDO::PARAM_STR,'lines' => PDO::PARAM_INT,'publish' => PDO::PARAM_BOOL);
     * $req = 'SELECT * FROM code WHERE language = :language AND lines = :lines AND publish = :publish';
     * You can bind $array like that :
     * bindArrayValue($array,$req,$typeArray);
     * The function is more useful when you use limit clause because they need an integer.
     * */
}
