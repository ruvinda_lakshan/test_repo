<?php
use Respect\Validation\Validator as v;




require(__DIR__ . '/../vendor/autoload.php');
require(__DIR__ . '/settings.php');
$app = new \Slim\App($settings);
$container = $app->getContainer();




$container['databaseAdapter'] = function($container)
{
  return new \Api\Adapters\DB\MySQLAdapter($container->get('settings')['db']);
};



$container['logger'] = function ($container) {
    $logger = new Monolog\Logger('logger');

    $formatter = new Monolog\Formatter\LineFormatter(
        "[%datetime%] [%level_name%]: %message% %context%\n",
        null,
        true,
        true
    );


    $rotating = new Monolog\Handler\RotatingFileHandler(__DIR__ . "/../logs/slim.log", 0, Monolog\Logger::DEBUG);
    $rotating->setFormatter($formatter);
    $logger->pushHandler($rotating);

    return $logger;
};

/*
$container["errorHandler"] = function ($container) {
    return new App\Handlers\ApiError($container["logger"]);
};
*/

// add exception handler to the container
$container['errorHandler'] = function ($container)
{
    return function ($request, $response, $exception) use ($container)
    {
        //Format of exception to return
        $data = (new \Api\Handlers\ExceptionHandler($exception))->handle();
        $container->logger->info('500 Error: '.json_encode($data['message']));
        //$response = (new \Api\Transformers\ResponseTransformer)->transform($response, $data, \Api\Transformers\ResponseTransformer::HTTP_SERVER_ERROR);
        //return $response->withHeader('Content-Type', 'application/json');
        return $response->withJson($data)->withHeader('Content-Type', 'application/json');
    };
};
$container['validator'] = function ($container) {
    return new Api\Validation\validator;
};

// assign objects that are needed across the app to $GLOBALS (NOTE: use with responsibility)
$GLOBALS['databaseAdapter'] = $container['databaseAdapter'];

/*
$container["token"] = function ($container) {
    return new Api\Token;
};
*/

$container["JwtAuthentication"] = function ($container) {

    return new JwtAuthentication([
        "path" => "/",
        "passthrough" => ["/token"],
        "secure"=>false,
        "secret" => getenv("JWT_SECRET"),
        "logger" => $container["logger"],
        "relaxed" => ["192.168.50.52"],
        "error" => function ($request, $response, $arguments) {
            $data["status"] = "error";
            $data["message"] = $arguments["message"];
            return $response
                ->withHeader("Content-Type", "application/json")
                ->write(json_encode($data, JSON_UNESCAPED_SLASHES | JSON_PRETTY_PRINT));
        },
        "callback" => function ($request, $response, $arguments) use ($container) {
            $container["token"]->hydrate($arguments["decoded"]);
        }
    ]);
};


$container['AuthController'] = function($container) {
    return new \Api\Controllers\Auth\AuthController($container);
};

$container['HelloController'] = function($container) {
    return new \Api\Controllers\HelloController($container);
};


$container['PassengerController'] = function($container) {
    return new \Api\Controllers\PassengerController($container);
};

$container['StationController'] = function($container) {
    return new \Api\Controllers\StationController($container);
};

// add middleware (NOTE: Last-In-First-Out order)
$app->add(new \Api\Middleware\TransformerMiddleware());

v::with('Api\\Validation\\Rules\\');

// call on routs (NOTE: a nifty way is used in the routs to call the controller class)
require(__DIR__ . '/routs.php');
