<?php

use Api\Controllers\Auth;
use Api\Middleware\AuthMiddleware;
use Api\Middleware\ValidationErrorsMiddleware;


$app->group('/v1',function () {


    $this->post('/auth/login','AuthController:postLogin');
    $this->post('/checkAuth','AuthController:authenticate');

    $this->post('/passenger/update','PassengerController:update');
    $this->post('/train/nearestStation','StationController:getNearestStation');
    $this->get('/hello/{name}', HelloController::class . ':index');


})->add(new AuthMiddleware($container));

$app->group('',function () {
    $this->post('/authenticate', 'AuthController:authenticate');

});

//print_r($container);
$app->group('',function () {
    $this->post('/passenger/register', 'PassengerController:register');
})->add(new ValidationErrorsMiddleware($container));