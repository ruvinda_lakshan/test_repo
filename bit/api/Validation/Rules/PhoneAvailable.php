<?php

namespace Api\Validation\Rules;



class PhoneAvailable extends \Respect\Validation\Rules\AbstractRule
{

    public function validate($input)
    {
        $passengerDAO=new \Api\Data\DAO\PassengerDAO();
        $isPhone=$passengerDAO->isPhoneExist((int)$input);



        if($isPhone)
        {
            return false;
        }
        return true;
    }
}