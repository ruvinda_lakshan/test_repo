<?php

namespace Api\Validation\Rules;



class EmailAvailable extends \Respect\Validation\Rules\AbstractRule
{

    public function validate($input)
    {
        $passengerDAO=new \Api\Data\DAO\PassengerDAO();
        $isEmail=$passengerDAO->isEmailExist($input);
        if($isEmail)
        {
            return false;
        }
        return true;
    }
}