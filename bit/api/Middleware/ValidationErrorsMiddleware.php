<?php

namespace Api\Middleware;
use Api\Handlers\ExceptionHandler;
use Respect\Validation\Validator as v;
class ValidationErrorsMiddleware extends Middleware
{

    public function __invoke($request,$response,$next)
    {
        $validation = $this->container->validator->validate($request,[
            'email' => v::noWhitespace()->notEmpty()->email()->emailAvailable(),
            'fname' => v::notEmpty(),
            'phone'=>v::noWhitespace()->notEmpty()->numeric()->phoneAvailable(),
            'password' => v::noWhitespace()->notEmpty(),

        ]);

        $errors=$validation->failed();
        if(isset($errors)){

            $payload = array (
                'code' => 'V-ERR',
                'message' => $errors,
                'result' => null
            );
            return $response->withJson(json_encode($payload));
        }

        $response = $next($request,$response);
        return $response;

    }
}