<?php


namespace Api\Middleware;

class AuthMiddleware extends Middleware
{

    public function __invoke($request,$response,$next)
    {
        if(!$this->container->AuthController->checkToken($request,$response)) {
            throw new \Exception('Token not defined');
        }
        $response = $next($request,$response);
        return $response;

    }
}